"""
Code for generating mock packets of the form specified by the iRobot Create
Open Interface specification for testing code when actually using a robot is
inconvenient.
"""
# Simple logging
import logging, sys
log = logging.getLogger(__name__)
_log_format = '%(asctime)s %(name)-12s %(levelname)-4s %(message)s'
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=_log_format)

import random
import struct
from cspy3 import Parser
from cspy3 import v1


class PacketGenerator:
    """A generator of mock packets for debugging."""
    FIRST_BYTE = 19
    def __init__(self, *sensor_ids):
        self.sensor_ids = sensor_ids
        self.setup()

    def setup(self):
        """Compute and set information for generating packets."""
        sensor_ids = self.sensor_ids
        # Get information necessary to build packets
        self.info   = [v1.PACKETS[x] for x in sensor_ids]
        self.sizes  = [x['size'] for x in self.info]
        self.names  = [x['name'] for x in self.info]
        self.dtypes = [x['dtype'] for x in self.info]
        self.ranges = [x['ValueRange'] for x in self.info]
        self.nbytes  = sum(self.sizes) + len(self.sensor_ids)
        self.format = ">BBB" + "B".join(self.dtypes) + "B"
        self.length = self.nbytes + 3

        log.debug('names: %s'%self.names)
        log.debug('sizes: %s'%self.sizes)
        log.debug('dtypes: %s'%self.dtypes)
        log.debug('format: %s'%self.format)
        log.debug('number of sensors: %d'%len(self.sensor_ids))
        log.debug('number of sensor bytes: %d'%self.nbytes)
        log.debug('packet length: %d'%self.length)

    def make_packet(self):
        """Create a packet, generating the data and converting it to bytes"""
        data = [self.FIRST_BYTE, self.nbytes]
        for sid, rng in zip(self.sensor_ids, self.ranges):
            val = random.randint(*rng)
            data.extend([sid, val])
        ret = bytearray(struct.pack(self.format[:-1], *data))
        ret.append(-sum(ret)%256)
        log.debug("Making packet from: %s"%data)
        return ret

    def __iter__(self):
        return self

    def __next__(self):
        return self.make_packet()