"""
A basic example to build on.

It illustrates the rough structure of how the code should work, but may have
errors/suboptimal patches because it has not been tested on the actual robots
yet.
"""
# Simple logging
import logging, sys
log = logging.getLogger(__name__)
_log_format = '%(asctime)s %(name)-12s %(levelname)-4s %(message)s'
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=_log_format)

import json
import os
import pty
import random
import select
import struct
import threading
import time
import serial

from collections import deque
from itertools import cycle
from threading import Thread

from cspy3 import Controller, Parser
from cspy3 import v1


class Flag(threading.Event):
    """A wrapper for the typical event class to allow for overriding the
    `__bool__` and `__call__` magic methods, since it looks nicer.
    """
    def __bool__(self):
        return self.is_set()

    def __call__(self):
        return self.is_set()


class StreamReader:
    """Reads streaming data using serial, passes it to a handler."""
    def __init__(self, stream, handler):
        log.debug("Creating StreamReader object for stream: %s"%stream)
        self.stream = stream
        self.handler = handler

    def run(self, halt_condition=None):
        """Begin the stream input processing loop."""
        if halt_condition is None:
            halt_condition = False

        try:
            log.debug("StreamReader started.")
            while not halt_condition:
                rlst, wlst, elst = select.select([self.stream], [], [], 0)

                if rlst:
                    data = self.stream.read()
                    # log.debug("read: %s"%data)
                    self.handler(*data)
                else:
                    time.sleep(0.001)
        finally:
            # Perform cleanup
            log.debug("StreamReader stopped.")


class Actor:
    """Acts on data that has been processed.
    """
    def __init__(self, feed, control=None, interval=1):
        log.debug("Creating Actor object")
        self.feed = feed
        self.control = control
        self.interval = interval

    def run(self, halt_condition=None):
        """Begin the action loop."""
        if halt_condition is None:
            halt_condition = False

        # wait for data to become available
        while not self.feed and not halt_condition:
            log.debug("Actor waiting for feed to become available")
            log.debug(self.feed)
            time.sleep(self.interval)
        else:
            if self.feed:
                obs = self.feed.pop()

        tic = time.time()
        while not halt_condition:
            while time.time() < tic:
                time.sleep(0.0001)
            tic += self.interval

            # Get the most recent data from the feed
            while self.feed:
                obs = self.feed.pop()

            # Do something with the information
            # print(json.dumps(obs, indent=2))
            # if obs['CliffLeftSignal'] < 1000 or obs['CliffFrontLeftSignal'] < 1000:
            #     self.control.drive_direct(-50, 50)
            # elif obs['CliffRightSignal'] < 1000 or obs['CliffFrontRightSignal'] < 1000:
            #     self.control.drive_direct(50, -50)
            # else:
            #     self.control.drive_direct(50, 50)
            if int(time.time())%2:
                self.control.drive_direct(50, -50)
            else:
                self.control.drive_direct(-50, 50)

        # Shut down the robot
        log.debug("Stopping robot control.")
        self.control.shutdown()



if __name__ == "__main__":
    import sys
    port_name = sys.argv[1]

    flag = Flag()
    try:
        sensor_ids = [28, 29, 30, 31]

        # initialize the robot
        controller = Controller(port_name)
        controller.mode_full()
        controller.request_stream(*sensor_ids)

        # get serial port for reader
        ser = controller.ser

        # the stream parser and output data store
        parser = Parser(*sensor_ids)
        output = parser.output

        # stream reader
        reader = StreamReader(ser, parser)

        # setup actor, giving it access to the controller
        actor = Actor(output, control=controller, interval=0.1)

        # Set up threads
        t_reader = Thread(target=reader.run, name="reader", args=(flag,))
        t_reader.start()

        t_actor = Thread(target=actor.run, name="actor", args=(flag,))
        t_actor.start()

        # List of all child threads
        threads = list(threading.enumerate()).remove(threading.main_thread())

        # Main thread just sleeps
        while True:
            time.sleep(0.1)


    except KeyboardInterrupt:
        log.debug("Interrupt received, setting quit flag.")
        flag.set()
    finally:
        # Ensure that the flag is set regardless since program is terminating
        log.debug("Starting termination, setting quit flag.")
        flag.set()

        # Join the threads
        log.debug("Attempting to join threads...")
        while threads:
            for t in threads:
                t.join(0.1)
                if t.is_alive():
                    log.debug("Thread %s not ready to join"%t.name)
                else:
                    log.debug("Thread %s successfully joined"%t.name)
                    threads.remove(t)
        log.debug("Program terminated.")