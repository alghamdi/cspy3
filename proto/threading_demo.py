#!/python3
"""
A demo of reading, parsing, and acting on a stream using Python 'threads'.
The goal is to be able to read from the stream when new data is available,
and share that data with the 'actor' so that it can use the most current 
information.
Here the main issue is being able to hand off control to the reader at 
appropriate intervals so that it can check whether the stream has new data.
Parsing the data is assumed to be quick, and the actor is assumed to be able
to make decisions within a relatively short timeframe, so both threads spend
most of their time idle.
If I/O is not the bottleneck (data acquisition or processing takes too long)
then an alternative method will likely be necessary.

For compactness, it also implements a stream writer using a pseudo tty.
"""
import io
import logging
import os
import pty
import select
import threading
import time
import serial

from collections import deque
from itertools import cycle
from threading import Thread


# Simple logging
logging.basicConfig(level=logging.DEBUG)
log = logging


class Flag(threading.Event):
    """A wrapper for the typical event class to allow for overriding the
    `__bool__` and `__call__` magic methods, since it looks nicer.
    """
    def __bool__(self):
        return self.is_set()

    def __call__(self):
        return self.is_set()


class StreamReader:
    """Reads streaming data using serial, stores the data in a deque."""
    def __init__(self, stream, buffer_length=1024, halt_condition=None):
        log.debug("Creating StreamHandler object for stream: %s"%stream)
        if halt_condition is None:
            halt_condition = False
        self.stream = stream 
        self.buffer = deque([-1], maxlen=buffer_length)
        self.halt_condition = halt_condition

    @classmethod
    def from_port(self, portname, *args, **kwargs):
        """Initialize the stream handler from a portname using `serial`."""
        # TODO: See docs for robot and serial module to ensure proper settings 
        stream = serial.Serial(portname, timeout=0)
        stream.nonblocking()
        return StreamHandler(stream, *args, **kwargs)
        
    def run(self):
        """Begin the stream input processing loop."""
        try:
            log.debug("StreamHandler started.")
            while not self.halt_condition:
                rlst, wlst, elst = select.select([self.stream], [], [], 0)

                if rlst:
                    data = self.stream.read()
                    log.debug("read: %s"%data)
                    val = int.from_bytes(data, 'big')
                    log.debug("appending: %s"%val)
                    self.buffer.append(val)
                else:
                    time.sleep(0.001)
        finally:
            # Perform cleanup
            log.debug("StreamHandler stopped.")

class Actor:
    """Example for objects/functions that take action based on buffer data."""
    def __init__(self, buffer, interval=1, halt_condition=None):
        log.debug("Creating Actor object")
        if halt_condition is None:
            halt_condition = False
        self.buffer = buffer
        self.interval = interval
        self.halt_condition = halt_condition

    def run(self):
        """Begin the action loop.""" 
        pass

if __name__ == "__main__":
    try:
        flag = Flag()

    except KeyboardInterrupt:
        log.debug("Interrupt received, setting quit flag.")
        flag.set()
    finally:
        # Ensure that the flag is set regardless since program is terminating
        log.debug("Starting termination, setting quit flag.")
        flag.set()

        # Join the threads
        log.debug("Attempting to join threads...")
        while threads:
            for t in threads:
                t.join(0.1)
                if t.is_alive():
                    log.debug("Thread %s not ready to join"%t.name)
                else:
                    log.debug("Thread %s successfully joined"%t.name)
                    threads.remove(t)
        log.debug("Program terminated.")