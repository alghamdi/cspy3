# cspy3 

Create Serial Port Packet Processor (CSP3) for the iRobot Create.



## Features

- Handle Create

## Install for development

```bash
git clone https://gitlab.com/rldotai/cspy3.git
cd cspy3
pip install --editable .
```

# TODO

- [x] Example of basic behavior using threads
- [x] Implement thread-safe parser w/ deque
- [ ] Test robot control using threads
- [ ] Profile and evaluate performance
- [ ] Implement worst-case acting loop changing.
- [x] Ensure safe closing of serial port when program exits
- [ ] Implement the remaining commands from Open Interface
- [ ] Log run information to file

## Improvements and Alternatives

- [ ] Implement simple command line demo w/ `argparse`
- [ ] Try using `multiprocessing` or `asyncio` instead of `threading`
- [ ] Implement state machine for validating packets
- [ ] Implement packet parser as a generator
- [ ] Add reflex code for dangerous situations or when no new data has been received for a certain period of time.
- [ ] Compatibility for Create V2 and iRobot Roombas.
- [ ] Incorporate webcam video feed
- [ ] Standardize logging

## License

MIT. See the [LICENSE](LICENSE) file for more details.