"""
General utilities for other modules.
"""

def clip(x, low, high):
    return max(min(x, high), low)