"""
Packet processing for the iRobot Create.

In the `stream` mode, the iRobot Create emits a stream of bytes representing 
sensory data. 
Parsing this stream requires a certain amount of flexibility, since different
sensors can be requested and not all sensor data is represented the same way.

Create Version 1 has packets of the form:
    19 <N> <id> <sensor> [<id> <sensor>, ...] <checksum>

Where `N` is the number of sensor bytes in the packet.
More information can be found at iRobot's website, but contrary to the 
documentation, the `start byte` (19) is included in the checksum calculation.
"""
import logging
import struct
import time 
from collections import deque

from .create_v1 import PACKETS


# Setup simple logging
log = logging.getLogger(__name__)


class Parser:
    """Parses the byte stream from the iRobot Create into sensory data.

    It accumulates bytes and, once enough bytes have been received, tries to 
    form a packet according to the Create V1 Open Interface specification.
    It then stores the packet data in a `dict` object, which is then appended to
    a `deque` (which is thread-safe and can be used as a circular buffer).
    """
    # Constants for the Create V1 protocol
    HEADER  = 19


    def __init__(self, *sensor_ids, timestamp=True):
        """
        Initialize the PacketParser. 

        Args:
            sensor_ids: List of the IDs for the sensors in the stream
        """
        log.debug("Initializing Parser...")
        self.sensor_ids = sensor_ids
        self.timestamp = timestamp

        # Get information about the sensors to prepare packet handling
        self.info   = [PACKETS[x] for x in self.sensor_ids]
        self.sizes  = [x['size'] for x in self.info]
        self.names  = [x['name'] for x in self.info]
        self.dtypes = [x['dtype'] for x in self.info]
        self.ranges = [x['ValueRange'] for x in self.info]
        self.count  = len(self.sensor_ids)
        self.nbytes = sum(self.sizes) + len(self.sensor_ids)
        self.format = ">BBB" + "B".join(self.dtypes) + "B"
        self.length = self.nbytes + 3

        # Set up data structures
        self.buffer = bytearray()
        self.output = deque(maxlen=1024)

    def __call__(self, *bseq):
        """Parse one or more bytes."""
        for b in bseq:
            self.input_byte(b)

    def input_byte(self, b):
        """Parse a single byte."""
        self.buffer.append(b)
        if len(self.buffer) > self.length:
            pkt = self.parse(self.buffer[:self.length])
            if pkt:
                # log.debug("Successfully parsed packet: %s"%pkt)
                self._store(pkt)
                self.buffer = self.buffer[self.length:]
            else:
                # log.error("Misaligned packet, discarding leading byte")
                self.buffer = self.buffer[1:]

    def parse(self, byteseq):
        """Parse `byteseq`, returning a `dict` containing the results of 
        parsing if the packet was valid or `None` otherwise.

        Args:
            byteseq: A bytearray or bytes object that may represent a packet.
        """
        log.debug("Parsing: %s"%list(bytearray(byteseq)))
        tic = time.time()
        # First check the length is right
        if len(byteseq) != self.length:
            log.error("Invalid length for byte_lst: %d \t expected: %d" %
                      (len(byteseq), self.length))
            return None

        try:
            # Unpack the bytes into the expected format  
            data = struct.unpack(self.format, byteseq)

            # Compute checksum
            chksum = sum(byteseq) % 256
            if chksum != 0:
                log.error("Invalid checksum: %d for input: %s"%(chksum, byteseq))
                return None 
            
            # Check header
            if data[0] != self.HEADER:
                log.error("Invalid packet header: %d \t expected: %d"%
                          (data[0], self.HEADER))
                return None
            if data[1] != self.nbytes:
                log.error("Invalid packet count: %d \t expected: %d"%
                          (data[1], self.nbytes))
                return None

            # Get sensor IDs and values
            ret = {}
            pairs = (data[i:i+2] for i in range(2, 2*self.count+1, 2))
            for i, (sid, val) in enumerate(pairs):
                # TODO: Check IDs and value ranges
                ret[self.names[i]] = val
            
            return ret
        except Exception as e:
            raise(e)
        finally:
            toc = time.time()
            log.debug("Parsing took: %f"%(toc-tic))
            
    def _store(self, pktdct):
        """Store the processed packet."""
        log.debug("Storing packet data: %s"%pktdct)
        self.output.appendleft(pktdct)